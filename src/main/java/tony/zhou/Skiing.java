package tony.zhou;

import tony.zhou.io.FileInput;
import tony.zhou.io.Input;
import tony.zhou.io.Output;
import tony.zhou.io.PrintStreamOutput;
import tony.zhou.model.Path;
import tony.zhou.solver.LongestPathSolver;
import tony.zhou.solver.Solver;

import java.io.IOException;
import java.net.URISyntaxException;

public class Skiing {
    public static void main(String[] args) throws IOException, URISyntaxException {
        Input input = new FileInput("map.txt");
        int[][] elevationMap = input.loadMap();

        Solver solver = new LongestPathSolver(elevationMap);
        Path path = solver.solve();

        Output output = new PrintStreamOutput(path, System.out);
        output.output();
    }
}
