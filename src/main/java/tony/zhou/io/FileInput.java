package tony.zhou.io;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileInput implements Input {
    private final Input delegate;

    public FileInput(String resourceName) throws IOException, URISyntaxException {
        delegate = new StringInput(new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource(resourceName).toURI()))));
    }

    @Override
    public int[][] loadMap() {
        return delegate.loadMap();
    }
}
