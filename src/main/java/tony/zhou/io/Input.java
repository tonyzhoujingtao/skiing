package tony.zhou.io;

public interface Input {
    int[][] loadMap();
}
