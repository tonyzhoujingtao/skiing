package tony.zhou.io;

import tony.zhou.model.Path;

import java.io.PrintStream;

public class PrintStreamOutput implements Output {

    private final Path path;
    private final PrintStream out;

    public PrintStreamOutput(Path path, PrintStream out) {
        this.path = path;
        this.out = out;
    }

    @Override
    public void output() {
        out.println(path);
        out.printf("Length=%s%n", path.getLength());
        out.printf("Drop=%s%n", path.getVerticalDrop());
    }
}
