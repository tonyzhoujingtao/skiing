package tony.zhou.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

public class StringInput implements Input {
    private final String rawMap;
    private int[][] map;

    public StringInput(String rawMap) {
        this.rawMap = rawMap;
    }

    @Override
    public int[][] loadMap() {
        if (mapIsNotReady()) {
            try (BufferedReader reader = new BufferedReader(new StringReader(rawMap))) {
                makeMap(reader);
                initMap(reader);
            } catch (IOException e) {
                throw new ReadRawMapException(e);
            }
        }

        return map;
    }

    private boolean mapIsNotReady() {
        return map == null || map.length == 0;
    }

    private void initMap(BufferedReader reader) throws IOException {
        for (int r = 0; r < map.length; r++) {
            String body = reader.readLine();
            String[] elevation = body.split(" ");
            for (int c = 0; c < map[0].length; c++) {
                map[r][c] = Integer.parseInt(elevation[c]);
            }
        }
    }

    private void makeMap(BufferedReader reader) throws IOException {
        String header = reader.readLine();
        String[] dimension = header.split(" ");
        int row = Integer.parseInt(dimension[0]);
        int col = Integer.parseInt(dimension[1]);
        map = new int[row][col];
    }

    private static class ReadRawMapException extends RuntimeException {
        public ReadRawMapException(Exception e) {
            super(e);
        }
    }
}
