package tony.zhou.model;

import com.google.common.base.Preconditions;

import java.util.Objects;

public final class Area {
    private final int row;
    private final int col;
    private final int elevation;

    public Area(int row, int col, int elevation) {
        Preconditions.checkArgument(row >= 0.0, "Negative row is not allowed: %s", row);
        Preconditions.checkArgument(col >= 0.0, "Negative col is not allowed: %s", col);
        Preconditions.checkArgument(elevation >= 0.0, "Negative elevation is not allowed: %s", elevation);

        this.row = row;
        this.col = col;
        this.elevation = elevation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRow(), getCol(), getElevation());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Area area = (Area) o;
        return Objects.equals(getRow(), area.getRow()) &&
                Objects.equals(getCol(), area.getCol()) &&
                Objects.equals(getElevation(), area.getElevation());
    }

    @Override
    public String toString() {
        return "Area{" +
                "row=" + row +
                ", col=" + col +
                ", elevation=" + elevation +
                '}';
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getElevation() {
        return elevation;
    }

    public boolean adjacentTo(Area area) {
        return isNorthOf(area) || isSouthOf(area) || isWestOf(area) || isEastOf(area);
    }

    private boolean isNorthOf(Area area) {
        return (row + 1 == area.row) && (col == area.col);
    }

    private boolean isSouthOf(Area area) {
        return (row - 1 == area.row) && (col == area.col);
    }

    private boolean isWestOf(Area area) {
        return (row == area.row) && (col + 1 == area.col);
    }

    private boolean isEastOf(Area area) {
        return (row == area.row) && (col - 1 == area.col);
    }

    public boolean isHigherThan(Area area) {
        return elevation > area.elevation;
    }
}
