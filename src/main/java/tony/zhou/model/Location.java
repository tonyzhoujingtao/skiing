package tony.zhou.model;

public final class Location {
    private final int row;
    private final int col;

    public Location(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Location goNorth() {
        return new Location(row - 1, col);
    }

    public Location goSouth() {
        return new Location(row + 1, col);
    }

    public Location goEast() {
        return new Location(row, col + 1);
    }

    public Location goWest() {
        return new Location(row, col - 1);
    }
}
