package tony.zhou.model;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

// Thread safe
public final class Path implements Comparable<Path> {
    private final List<Area> areas;
    private final AtomicBoolean isLongest;

    public Path() {
        this(new ArrayList<>());
    }

    public Path(List<Area> areas) {
        this.areas = new CopyOnWriteArrayList<>(areas);
        isLongest = new AtomicBoolean(false);
    }

    public void addArea(Area area) {
        Preconditions.checkArgument(getEnd() == null || getEnd().adjacentTo(area), "New area must be adjacent to the end of the path: New Area=%s; Path End=%s", area, getEnd());
        Preconditions.checkArgument(getEnd() == null || getEnd().isHigherThan(area), "New area must be lower than the end of the path: New Area=%s; Path End=%s", area, getEnd());
        areas.add(area);
    }

    public Area getEnd() {
        if (areas.isEmpty()) {
            return null;
        }
        return areas.get(areas.size() - 1);
    }

    public Area getStart() {
        if (areas.isEmpty()) {
            return null;
        }
        return areas.get(0);
    }

    public boolean isLongest() {
        return isLongest.get();
    }

    public void setIsLongest(boolean isLongest) {
        this.isLongest.set(isLongest);
    }

    public void addSubPath(Path subPath) {
        areas.addAll(subPath.getAreas());
    }

    public List<Area> getAreas() {
        return new ArrayList<>(areas);
    }

    public int getEndElevation() {
        if (getEnd() == null) {
            return 0;
        }
        return getEnd().getElevation();
    }

    @Override
    public int compareTo(Path o) {
        if (getLength() == o.getLength()) {
            return getVerticalDrop() - o.getVerticalDrop();
        }

        return getLength() - o.getLength();
    }

    public int getLength() {
        return areas.size();
    }

    public int getVerticalDrop() {
        if (areas.isEmpty()) {
            return 0;
        }
        return areas.get(0).getElevation() - areas.get(areas.size() - 1).getElevation();
    }

    @Override
    public String toString() {
        return String.join("-", areas.stream().map(a -> String.valueOf(a.getElevation())).collect(Collectors.toList()));
    }
}
