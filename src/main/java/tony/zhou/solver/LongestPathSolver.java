package tony.zhou.solver;

import com.google.common.collect.Lists;
import tony.zhou.model.Area;
import tony.zhou.model.Location;
import tony.zhou.model.Path;

import java.util.List;
import java.util.stream.Collectors;

public class LongestPathSolver implements Solver {
    private final Path[][] pathMap;

    public LongestPathSolver(int[][] elevationMap) {
        this.pathMap = new Path[elevationMap.length][elevationMap[0].length];
        for (int r = 0; r < pathMap.length; r++) {
            for (int c = 0; c < pathMap[0].length; c++) {
                pathMap[r][c] = new Path(Lists.newArrayList(new Area(r, c, elevationMap[r][c])));
            }
        }
    }

    @Override
    public Path solve() {
        Path longestPath = null;

        for (int r = 0; r < pathMap.length; r++) {
            for (int c = 0; c < pathMap[0].length; c++) {
                Path path = solve(new Location(r, c));
                if (longestPath == null || longestPath.compareTo(path) < 0) {
                    longestPath = path;
                }
            }
        }

        return longestPath;
    }

    private Path solve(Location location) {
        final int r = location.getRow();
        final int c = location.getCol();

        if (!pathMap[r][c].isLongest()) {
            List<Path> subPaths = Lists.newArrayList();

            checkNorth(location, subPaths);
            checkSouth(location, subPaths);
            checkEast(location, subPaths);
            checkWest(location, subPaths);

            if (!subPaths.isEmpty()) {
                pathMap[r][c].addSubPath(findLongestPath(subPaths));
            }
            pathMap[r][c].setIsLongest(true);
        }

        return pathMap[r][c];
    }

    private Path findLongestPath(List<Path> paths) {
        return paths.stream().sorted((p1, p2) -> {
            int lenDiff = p2.getLength() - p1.getLength();

            if (lenDiff == 0) {
                return p1.getEndElevation() - p2.getEndElevation();
            }

            return lenDiff;
        }).collect(Collectors.toList()).get(0);
    }


    private void checkWest(Location location, List<Path> subPaths) {
        if (canGoWest(location)) {
            subPaths.add(solve(location.goWest()));
        }
    }

    private boolean canGoWest(Location location) {
        Location west = location.goWest();
        return (west.getCol() >= 0) &&
                (pathMap[location.getRow()][location.getCol()].getEnd().isHigherThan(pathMap[west.getRow()][west.getCol()].getStart()));
    }


    private void checkEast(Location location, List<Path> subPaths) {
        if (canGoEast(location)) {
            subPaths.add(solve(location.goEast()));
        }
    }

    private boolean canGoEast(Location location) {
        Location east = location.goEast();
        return (east.getCol() < pathMap[0].length) &&
                (pathMap[location.getRow()][location.getCol()].getEnd().isHigherThan(pathMap[east.getRow()][east.getCol()].getStart()));
    }


    private void checkSouth(Location location, List<Path> subPaths) {
        if (canGoSouth(location)) {
            subPaths.add(solve(location.goSouth()));
        }
    }

    private boolean canGoSouth(Location location) {
        Location south = location.goSouth();
        return (south.getRow() < pathMap.length) &&
                (pathMap[location.getRow()][location.getCol()].getEnd().isHigherThan(pathMap[south.getRow()][south.getCol()].getStart()));
    }

    private void checkNorth(Location location, List<Path> subPaths) {
        if (canGoNorth(location)) {
            subPaths.add(solve(location.goNorth()));
        }
    }

    private boolean canGoNorth(Location location) {
        Location north = location.goNorth();
        return (north.getRow() >= 0) &&
                (pathMap[location.getRow()][location.getCol()].getEnd().isHigherThan(pathMap[north.getRow()][north.getCol()].getStart()));
    }
}
