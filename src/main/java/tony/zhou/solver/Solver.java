package tony.zhou.solver;

import tony.zhou.model.Path;

public interface Solver {
    Path solve();
}
