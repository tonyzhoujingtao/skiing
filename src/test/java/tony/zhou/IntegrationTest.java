package tony.zhou;

import org.junit.Test;
import tony.zhou.io.FileInput;
import tony.zhou.model.Path;
import tony.zhou.solver.LongestPathSolver;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class IntegrationTest {

    @Test
    public void solve_sampleMap() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map1.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(5, path.getLength());
        assertEquals(8, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(15, path.getLength());
        assertEquals(1422, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap2() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map2.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(5, path.getLength());
        assertEquals(8, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap3() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map3.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(6, path.getLength());
        assertEquals(8, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap4() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map4.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(5, path.getLength());
        assertEquals(8, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap5() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map5.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(6, path.getLength());
        assertEquals(7, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap6() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map6.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(5, path.getLength());
        assertEquals(7, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap7() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map7.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(6, path.getLength());
        assertEquals(8, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap8() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map8.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(16, path.getLength());
        assertEquals(15, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

    @Test
    public void solve_realMap9() throws IOException, URISyntaxException {
        FileInput fileInput = new FileInput("map9.txt");
        LongestPathSolver solver = new LongestPathSolver(fileInput.loadMap());

        Path path = solver.solve();

        assertEquals(6, path.getLength());
        assertEquals(7, path.getVerticalDrop());

        System.out.println(path.getAreas());
    }

}