package tony.zhou.io;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class FileInputTest {

    @Test
    public void loadMap_of4X4Matrix_return4X4Array() throws IOException, URISyntaxException {
        FileInput testObject = new FileInput("map1.txt");

        int[][] map = testObject.loadMap();

        assertEquals(4, map.length);
        assertEquals(4, map[0].length);
        assertEquals(4, map[0][0]);
        assertEquals(6, map[3][3]);
    }

    @Test
    public void loadMap_of1000X1000Matrix_return1000X1000Array() throws IOException, URISyntaxException {
        FileInput testObject = new FileInput("map.txt");

        int[][] map = testObject.loadMap();

        assertEquals(1000, map.length);
        assertEquals(1000, map[0].length);
        assertEquals(50, map[0][0]);
        assertEquals(1460, map[999][999]);
    }

}