package tony.zhou.io;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringInputTest {

    @Test
    public void loadMap_of1X1Matrix_return1X1Array() {
        StringInput testObject = new StringInput("1 1\n100");

        int[][] map = testObject.loadMap();

        assertEquals(1, map.length);
        assertEquals(1, map[0].length);
        assertEquals(100, map[0][0]);
    }

    @Test
    public void loadMap_of2X2Matrix_return2X2Array() {
        StringInput testObject = new StringInput(
                "2 2\n"
                        + "100 101\n"
                        + "200 201");

        int[][] map = testObject.loadMap();

        assertEquals(2, map.length);
        assertEquals(2, map[0].length);
        assertEquals(201, map[1][1]);
    }

    @Test
    public void loadMap_of3X3Matrix_return3X3Array() {
        StringInput testObject = new StringInput(
                "3 3\n"
                        + "100 101 102\n"
                        + "200 201 202\n"
                        + "300 301 302\n");

        int[][] map = testObject.loadMap();

        assertEquals(3, map.length);
        assertEquals(3, map[0].length);
        assertEquals(302, map[2][2]);
    }

}