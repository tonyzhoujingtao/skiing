package tony.zhou.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AreaTest {

    @Test(expected = IllegalArgumentException.class)
    public void initialize_withNegativeRow_throwException() {
        new Area(-1, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initialize_withNegativeCol_throwException() {
        new Area(1, -1, 1);
    }


    @Test(expected = IllegalArgumentException.class)
    public void initialize_withNegativeElevation_throwException() {
        new Area(1, 1, -1);
    }

    @Test
    public void adjacentTo_areaInNorth_returnTrue() {
        Area orig = new Area(1, 1, 10);
        Area north = new Area(0, 1, 10);

        assertTrue(orig.adjacentTo(north));
    }

    @Test
    public void adjacentTo_areaInSouth_returnTrue() {
        Area orig = new Area(1, 1, 10);
        Area south = new Area(2, 1, 10);

        assertTrue(orig.adjacentTo(south));
    }

    @Test
    public void adjacentTo_areaInEast_returnTrue() {
        Area orig = new Area(1, 1, 10);
        Area east = new Area(1, 2, 10);

        assertTrue(orig.adjacentTo(east));
    }

    @Test
    public void adjacentTo_areaInWest_returnTrue() {
        Area orig = new Area(1, 1, 10);
        Area west = new Area(1, 0, 10);

        assertTrue(orig.adjacentTo(west));
    }

    @Test
    public void adjacentTo_areaInNorthWest_returnFalse() {
        Area orig = new Area(1, 1, 10);
        Area northWest = new Area(0, 0, 10);

        assertFalse(orig.adjacentTo(northWest));
    }


    @Test
    public void isHigherThan_aLowerArea_returnTrue() {
        Area orig = new Area(1, 1, 10);
        Area lower = new Area(0, 0, 9);

        assertTrue(orig.isHigherThan(lower));
    }

    @Test
    public void isHigherThan_aSameHeightArea_returnFalse() {
        Area orig = new Area(1, 1, 10);
        Area sameHeight = new Area(0, 0, 10);

        assertFalse(orig.isHigherThan(sameHeight));
    }

    @Test
    public void isHigherThan_aHigherArea_returnFalse() {
        Area orig = new Area(1, 1, 10);
        Area higher = new Area(0, 0, 10);

        assertFalse(orig.isHigherThan(higher));
    }


}