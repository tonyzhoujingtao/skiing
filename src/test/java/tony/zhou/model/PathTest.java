package tony.zhou.model;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PathTest {

    private Path testObject;

    @Test
    public void addArea_whenNormal_appendAreaToTheEndOfPath() {
        testObject = makePath();
        Area area1 = new Area(0, 0, 10);
        Area area2 = new Area(0, 1, 9);

        testObject.addArea(area1);
        testObject.addArea(area2);

        List<Area> areas = testObject.getAreas();
        assertEquals(area2, areas.get(areas.size() - 1));
    }

    private Path makePath() {
        return new Path();
    }

    @Test(expected = IllegalArgumentException.class)
    public void addArea_notAdjacentToTheEndOfThePath_throwException() {
        testObject = makePath();
        Area area1 = new Area(1, 1, 10);
        Area area2 = new Area(0, 2, 11);

        testObject.addArea(area1);
        testObject.addArea(area2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addArea_sameHeightAsTheEndOfThePath_throwException() {
        testObject = makePath();
        Area area1 = new Area(1, 1, 10);
        Area area2 = new Area(1, 2, 10);

        testObject.addArea(area1);
        testObject.addArea(area2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addArea_higherThanTheEndOfThePath_throwException() {
        testObject = makePath();
        Area area1 = new Area(1, 1, 10);
        Area area2 = new Area(1, 2, 11);

        testObject.addArea(area1);
        testObject.addArea(area2);
    }

    @Test
    public void getLength_ofEmptyPath_returnZero() {
        testObject = makePath();

        int length = testObject.getLength();

        assertEquals(0, length);
    }

    @Test
    public void getLength_ofPathWithOneArea_returnOne() {
        testObject = makePath();
        testObject.addArea(new Area(1, 1, 10));

        int length = testObject.getLength();

        assertEquals(1, length);
    }

    @Test
    public void getLength_ofPathWithTwoAreas_returnOne() {
        testObject = makePath();
        testObject.addArea(new Area(1, 1, 10));
        testObject.addArea(new Area(1, 2, 9));

        int length = testObject.getLength();

        assertEquals(2, length);
    }

    @Test
    public void getVerticalDrop_ofEmptyPath_returnZero() {
        testObject = makePath();

        int verticalDrop = testObject.getVerticalDrop();

        assertEquals(0, verticalDrop);
    }

    @Test
    public void getVerticalDrop_ofPathWithOneArea_returnZero() {
        testObject = makePath();

        int verticalDrop = testObject.getVerticalDrop();

        assertEquals(0, verticalDrop);
    }

    @Test
    public void getVerticalDrop_ofPathWithTwoAreas_returnDifferenceOfHeadAndEnd() {
        testObject = makePath();
        testObject.addArea(new Area(1, 1, 10));
        testObject.addArea(new Area(1, 2, 9));

        int verticalDrop = testObject.getVerticalDrop();

        assertEquals(1, verticalDrop);
    }

    @Test
    public void getVerticalDrop_ofPathWithThreeAreas_returnDifferenceOfHeadAndEnd() {
        testObject = makePath();
        testObject.addArea(new Area(1, 1, 10));
        testObject.addArea(new Area(1, 2, 9));
        testObject.addArea(new Area(1, 3, 1));

        int verticalDrop = testObject.getVerticalDrop();

        assertEquals(9, verticalDrop);
    }


}