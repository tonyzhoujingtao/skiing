package tony.zhou.solver;

import org.junit.Test;
import tony.zhou.model.Path;

import static org.junit.Assert.assertEquals;

public class LongestPathSolverTest {
    @Test
    public void solve_1X1Map_returnPathOfOneArea() {
        LongestPathSolver testObject = new LongestPathSolver(new int[][]{{1}});

        Path path = testObject.solve();

        assertEquals(1, path.getLength());
        assertEquals(0, path.getVerticalDrop());
    }

    @Test
    public void solve_2X2MapWithSameElevation_returnPathOfOneArea() {
        LongestPathSolver testObject = new LongestPathSolver(new int[][]{{2, 2}, {2, 2}});

        Path path = testObject.solve();

        assertEquals(1, path.getLength());
        assertEquals(0, path.getVerticalDrop());
    }

    @Test
    public void solve_2X2MapWithTwoAdjacentDecreasingElevations_returnPathOfTwoAreas() {
        LongestPathSolver testObject = new LongestPathSolver(new int[][]{{2, 1}, {2, 2}});

        Path path = testObject.solve();

        assertEquals(2, path.getLength());
        assertEquals(1, path.getVerticalDrop());
    }


    @Test
    public void solve_2X2MapWithFourAdjacentDecreasingElevations_returnPathOfFourAreas() {
        LongestPathSolver testObject = new LongestPathSolver(new int[][]{{1, 2}, {4, 3}});

        Path path = testObject.solve();

        assertEquals(4, path.getLength());
        assertEquals(3, path.getVerticalDrop());
    }

    @Test
    public void solve_2X2MapWithThreeAdjacentDecreasingElevations_returnPathOfThreeAreas() {
        LongestPathSolver testObject = new LongestPathSolver(new int[][]{{4, 3}, {2, 1}});

        Path path = testObject.solve();

        assertEquals(3, path.getLength());
        assertEquals(3, path.getVerticalDrop());
    }
}